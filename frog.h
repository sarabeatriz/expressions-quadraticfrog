#ifndef FROG_H
#define FROG_H

#include <QWidget>
#include <QPainter>
#include <QtGui>
#include <QtCore>
#include <QTimer>
#include <QLabel>

class frog : public QWidget
{
    Q_OBJECT
public:
    explicit frog(QWidget *parent = 0);
    float Ysapo, Xsapo, initialX;
    bool flag, xFlag, backFlag, moved, showText;

    // coefficients from the quadratic formula
    int A, B, C;

    int temp,temp2,xnenu,xnenu2,xnenu3;

    double QuadraticPlus(int a, int b, int c);
    double QuadraticMinus(int a, int b, int c);
    void computeNenuPositions();
    ~frog();

private:
    QLabel *inter1Label;
    QLabel *inter2Label;

signals:

public slots:
    void animate();
    void run(int, int, int);
    void reset();


protected:
    void paintEvent(QPaintEvent *event);
    QTimer *myTimer;

};

#endif // FROG_H
